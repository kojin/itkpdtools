import itkdb
import json
import pprint
import getpass
import sys 
import datetime
import dbtools

from pathlib import Path
from argparse import ArgumentParser


def get_option():
    argparser = ArgumentParser()
    argparser.add_argument('-s', '--serialNumber', type=str,
                           required=True,default='',
                           help='specify serial nubmer')
    argparser.add_argument('-f', '--fromLoc', type=str,
                           required=True,default='',
                           help='from : source location')
    argparser.add_argument('-t', '--toLoc', type=str,
                           required=True,default='',
                           help='to : destination')
    return argparser.parse_args()


if __name__ == '__main__':
    args = get_option()
    serialNumbers=[args.serialNumber]
    sourceLocation=args.fromLoc
    destLocation=args.toLoc

    if dbtools.confirm() :
        code1 = getpass.getpass(prompt='access_code1: ')
        code2 = getpass.getpass(prompt='access_code2: ')
        pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
        pd_client = itkdb.Client( user = pd_user, use_eos = True )
        record=dbtools.shipComp(pd_client,serialNumbers,sourceLocation,destLocation,
                         f'Sensor Wafer Shipping HPK -> KEK {datetime.datetime.now().strftime("%Y-%m-%d")}')
        
