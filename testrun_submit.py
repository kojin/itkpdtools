import itkdb
import json
import pprint
import getpass
import sys 
import dbtools

from pathlib import Path
from argparse import ArgumentParser

def get_option():
    argparser = ArgumentParser()
    argparser.add_argument('-f', '--fileName', type=str,
                           required=True,
                           help='Input json file name')
    return argparser.parse_args()

if __name__ == '__main__':
    args = get_option()

    filename = args.fileName
    pprint.pprint(filename)

    json_open=open(filename,'r')
    results = json.load(json_open)
    pprint.pprint(results)

    serialNumber=results['component']
    try :
        uploadStage=results['stage']
    except:
        uploadStage=''
        
    pprint.pprint(serialNumber)

    code1 = getpass.getpass(prompt='access_code1: ')
    code2 = getpass.getpass(prompt='access_code2: ')

    pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
    pd_client = itkdb.Client( user = pd_user, use_eos = True )

    if uploadStage != '' :
        compStage=dbtools.getCompStage(pd_client,serialNumber)
        dbtools.changestage(pd_client,compStage,uploadStage)

    if dbtools.confirm(results):
        dbtools.uploadTestRun(pd_client,results)

