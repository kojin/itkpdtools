#########################################################################
# Andreas Gisen, Simon Huiberts, Giordon Stark UiO,UiB,CERN             #
# andreas.gisen@tu-dortmund.de, shu011@uib.no, GSTARK@CERN.CH           #
# October 2020                                                          #
#########################################################################
# -*- coding: utf-8 -*-
import json
import os
import sys
from os.path import basename
from os.path import dirname

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.use("TkAgg")

try:
    input = raw_input
except NameError:
    pass


def coloredFlag(flag):
    if flag:
        return "Yes, \033[1;32m[PASSED]\x1b[0m"
    else:
        return "No, \033[1;31m[FAILED]\x1b[0m"


def analyseIV(argv):

    for data_files in argv:
        fig, ax = plt.subplots(1, figsize=(7.2, 4.0))

        # Open json file and read in the data
        with open(data_files, "r") as data:
            data_file = json.load(data)
            db_sensorID = data_file["component"]
            db_measType = data_file["test"]
            if db_measType != "IV":
                raise Exception("Not an IV file!")
            db_institute = data_file["institution"]
            db_date = data_file["date"]
            runNumber = data_file["runNumber"] if "runNumber" in data_file else None
            db_prefix = data_file["prefix"]
            passed = data_file["passed"] if "passed" in data_file else None
            Ilc0 = data_file["Ilc"] if "Ilc" in data_file else 0
            Vbd0 = data_file["Vbd"] if "Vbd" in data_file else 0
            tilesize = data_file["size"] if "size" in data_file else 0
            Vdepl = data_file["depletion_voltage"]
            # If temp/hum properties missing, calculate from average
            try:
                db_temperature = data_file["properties"]["TEMP"]
                db_humidity = data_file["properties"]["HUM"]
            except IOError:
                num_temp = np.array(data_file["IV_ARRAY"]["temperature"])
                num_hum = np.array(data_file["IV_ARRAY"]["humidity"])
                db_temperature = np.average(num_temp)
                db_humidity = np.average(num_hum)

            iv_data = {"t": [], "U": [], "Iavg": [], "Istd": [], "T": [], "RH": []}

            iv_data["U"] = data_file["IV_ARRAY"]["voltage"]
            iv_data["Iavg"] = data_file["IV_ARRAY"]["current"]
            try:
                timedata = iv_data["t"] = data_file["IV_ARRAY"]["time"]
                iv_data["Istd"] = data_file["IV_ARRAY"]["sigma current"]
                tempdata = iv_data["T"] = data_file["IV_ARRAY"]["temperature"]
                humidata = iv_data["RH"] = data_file["IV_ARRAY"]["humidity"]
            except IOError:
                timedata = iv_data["t"]
                iv_data["Istd"]
                tempdata = iv_data["T"]
                humidata = iv_data["RH"]

            # Converting to absolute values
            xdata = [abs(x) for x in iv_data["U"]]
            ydata = [abs(x) for x in iv_data["Iavg"]]
            yerr = [abs(x) for x in iv_data["Istd"]]

            # Convert to "uA" if data is in A
            if db_prefix == "A":
                ydata = [x * 1e6 for x in ydata]
                yerr = [x * 1e6 for x in yerr]

        # # Finding Leakage current and breakdown voltage
        # if Vdepl == 0:
        #    Vdepl = float(input('Please enter the depletion voltage (in V) for sensor "{}":\n'.format(db_sensorID)))

        # Checking if the component is 3D by using YY-identifiers
        if any(db_sensorID[6] == x for x in ["0", "1", "V", "W"]):
            print("Setting Criterias for 3D sensor")
            is3D = True

        else:
            print("Setting Criterias for planar sensor")
            is3D = False

        # 3D sensor criterias
        if is3D:
            if 0 < Vdepl < 10:
                Vdepl_flag = True
            else:
                Vdepl_flag = False
            I_voltage_point = Vdepl + 20
            break_threshold = Vdepl + 20
            I_treshold = 2.5  # current in uA

        # Planar sensor criterias
        else:
            # Depletion voltage criteria for 100um thick sensors
            if (0 < Vdepl < 60) and any(db_sensorID[6] == x for x in ["2", "T"]):
                Vdepl_flag = True

            # Depletion voltage criteria for 150um thick sensors
            elif (0 < Vdepl < 100) and any(db_sensorID[6] == x for x in ["3", "U"]):
                Vdepl_flag = True
            else:
                Vdepl_flag = False

            I_voltage_point = Vdepl + 50
            break_threshold = Vdepl + 70
            I_treshold = 0.75  # current in uA

        # area of sensitive area
        if any(db_sensorID[6] == x for x in ["0", "1"]):
            area = 4.25  # cm^2 for 3D
        elif db_sensorID[6] == "2":
            area = 15.76  # cm^2 for inner Quad
        elif db_sensorID[6] == "3":
            area = 15.92  # cm^2 for Outer Quad
        # Test structure
        elif any(db_sensorID[6] == x for x in ["T", "U", "V", "W"]):
            area = tilesize
            # if area == 0:
            #    area = float(input("Enter test structure area to continue: ")) # nosec
        # else:
        #    print("No area found on your pixel sensor")
        #    area = float(input("Enter sensor area to continue: ")) # nosec

        # checks on input data if available
        print("{:48} {}".format("Sensor ID:", db_sensorID))
        if passed is not None:
            print("{:48} {}".format("Input status (Passed or not):", passed))
        if Vdepl == 0:
            print("{:48}".format("Input depletion voltage: {}V)?".format(Vdepl)))
        else:
            print(
                "{:48} {}".format(
                    "Input depletion voltage: {}V)?".format(Vdepl),
                    coloredFlag(Vdepl_flag),
                )
            )
        Vbd0_flag = Vbd0 > break_threshold
        if Vbd0 == 0:
            print("{:48}".format("Input Breakdown voltage {} V".format(Vbd0)))
        else:
            print(
                "{:48} {}".format(
                    "Input Breakdown voltage {} V, in excess of {} V?".format(
                        Vbd0, break_threshold
                    ),
                    coloredFlag(Vbd0_flag),
                )
            )

        Ilc0_flag = False
        Ilc0_uA_cm = 0
        print("area", area)
        if area > 0:
            Ilc0_uA_cm = (Ilc0) / area
            Ilc0_uA_cm = round(Ilc0_uA_cm, 4)

            # Pass or fail on leakaged current
            Ilc0_flag = Ilc0_uA_cm < I_treshold

        if Ilc0 == 0:
            print("{:48}".format("Input Leakage current {} uA.".format(Ilc0)))
        else:
            print(
                "{:48} {}".format(
                    "Input Leakage current {} uA, per area {} uA/cm2 pass?".format(
                        Ilc0, Ilc0_uA_cm
                    ),
                    coloredFlag(Ilc0_flag),
                )
            )

        Vbd = 0
        Ilc = 0
        Vlc = 0
        # Finding leakage current at threshold voltage
        for idx, V in enumerate(xdata):
            if V < Vdepl:
                continue
            elif V <= I_voltage_point:
                Vlc = V
                Ilc = ydata[idx]

            # Finding breakdown voltage for 3D
            if is3D:
                if ydata[idx] > ydata[idx - 5] * 2 and xdata[idx - 5] > Vdepl:
                    Vbd = xdata[idx - 5]
                    print("Breakdown at {:.1f} V for 3D sensor".format(Vbd))
                    ax.axvline(
                        Vbd, linewidth=4, color="r", label="Bd @ {:.0f}V".format(Vbd)
                    )
                    break

            # Finding breakdown voltage for Planar
            else:
                if ydata[idx] > ydata[idx - 1] * 1.2 and xdata[idx - 1] != 0:
                    Vbd = V
                    print("Breakdown at {:.1f} V for planar sensor".format(Vbd))
                    ax.axvline(
                        Vbd, linewidth=4, color="r", label="Bd @ {:.0f}V".format(Vbd)
                    )
                    break

        Ilc_flag = False
        Ilc_uA_cm = 0
        if area > 0:
            Ilc_uA_cm = (Ilc) / area
            Ilc_uA_cm = round(Ilc_uA_cm, 4)

            # Pass or fail on leakaged current
            Ilc_flag = Ilc_uA_cm < I_treshold

        # Pass on breakdown voltage check if calculate Vbd > threshold
        # (give a pass if Vbd not found but the max. measured voltage> threshold)
        Vbd_flag = (Vbd > break_threshold) or (Vbd == 0 and xdata[-1] > break_threshold)

        # give pass if the input values pass the criteria
        total_flag = (Vbd_flag or Vbd0_flag) and (Ilc_flag or Ilc0_flag) and Vdepl_flag

        # Printing and plotting
        print("Calculation from input depletion voltage and IV data:")
        if Vbd > 0:
            print(
                "{:48} {}".format(
                    "Breakdown voltage: {}V, In excess of {:.0f}V?".format(
                        Vbd, break_threshold
                    ),
                    coloredFlag(Vbd_flag),
                )
            )
        else:
            print(
                "{:48} {}".format(
                    "Breakdown voltage > {}V, In excess of {:.0f}V?".format(
                        xdata[-1], break_threshold
                    ),
                    coloredFlag(Vbd_flag),
                )
            )
        print(
            "{:48} {} uA".format(
                "Leakage current at {} (={:.0f}V):".format(I_voltage_point, Vlc), Ilc
            )
        )
        print(
            "{:48} {}".format(
                "Leakage current {} uA/cm2 pass?".format(Ilc_uA_cm),
                coloredFlag(Ilc_flag),
            )
        )

        print(
            "{:48} {}".format(
                "Does the sensor meet all IV criteria?", coloredFlag(total_flag)
            ),
            "\n",
        )

        # Plotting options
        if len(yerr) == 0:
            p1 = ax.plot(xdata[1:], ydata[1:], label="current")
            first_legend = plt.legend(
                handles=p1, loc="lower center", bbox_to_anchor=(0.15, -0.33)
            )
            plt.gca().add_artist(first_legend)
        else:
            p1 = ax.errorbar(
                xdata[1:], ydata[1:], yerr=yerr[1:], fmt="ko", label="current"
            )
            first_legend = plt.legend(
                handles=[p1], loc="lower center", bbox_to_anchor=(0.15, -0.33)
            )
            plt.gca().add_artist(first_legend)

        if len(tempdata) == 0:
            print("No temperature array given")
        elif len(xdata[1:]) == len(iv_data["T"][1:]):
            ax1 = ax.twinx()
            (p2,) = ax1.plot(
                xdata[1:], iv_data["T"][1:], color="C1", label="temperature"
            )
            ax1.set_ylabel("T [°C]", color="C1", fontsize="large")
            second_legend = plt.legend(
                handles=[p2], loc="lower center", bbox_to_anchor=(0.5, -0.33)
            )
            plt.gca().add_artist(second_legend)

        if len(humidata) == 0:
            print("No humidity array given")
        elif len(xdata[1:]) == len(iv_data["RH"][1:]):
            ax2 = ax.twinx()
            (p3,) = ax2.plot(xdata[1:], iv_data["RH"][1:], color="C2", label="humidity")
            ax2.set_ylabel("RH [%]", color="C2", fontsize="large")
            ax2.spines["right"].set_position(("outward", 60))
            third_legend = plt.legend(
                handles=[p3], loc="lower center", bbox_to_anchor=(0.85, -0.33)
            )
            plt.gca().add_artist(third_legend)

        ax.set_title('IV for sensor "{}"'.format(db_sensorID), fontsize="large")
        # ax.set_title('IV for sensor "{}", {}'.format(db_sensorID, '[PASSED]' if total_flag else '[FAILED]'), fontsize='large')
        ax.set_xlabel("U [V]", ha="right", va="top", x=1.0, fontsize="large")
        ax.set_ylabel("I [uA]", ha="right", va="bottom", y=1.0, fontsize="large")
        fig.subplots_adjust(bottom=0.25)
        fig.subplots_adjust(right=0.75)

        ax.grid()

        pathname = dirname(os.getcwd()) + "/plots/"
        if not os.path.isdir(pathname):
            os.makedirs(pathname)
        namefile = basename(data_files)
        savename = os.path.join(pathname, os.path.splitext(namefile)[0])

        fig.savefig(savename + ".pdf".format(), dpi=300)
        fig.savefig(savename + ".png".format(), dpi=300)

        ax.set_yscale("log")

        fig.savefig(savename + "_log.pdf".format(), dpi=300)
        fig.savefig(savename + "_log.png".format(), dpi=300)

        plt.close("all")

        plot_path = savename + ".png"

        return (
            db_sensorID,
            db_institute,
            db_date,
            runNumber,
            db_humidity,
            db_temperature,
            timedata,
            xdata,
            ydata,
            yerr,
            tempdata,
            humidata,
            Vbd,
            Ilc,
            plot_path,
            total_flag,
        )


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Please provide database file(s) as argument(s).")
    else:
        analyseIV(sys.argv[1:])
