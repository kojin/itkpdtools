import itkdb
import json
import pprint
import getpass
import sys 
import dbtools

from pathlib import Path
from argparse import ArgumentParser

def get_option():
    argparser = ArgumentParser()
    argparser.add_argument('-f', '--fileName', type=str,
                           required=True,
                           help='Input json file name')
    argparser.add_argument('-s', '--serialNumber', type=str,
                           required=False,default='',
                           help='specify serial nubmer')
    return argparser.parse_args()

def read_data(filename,debug=False):
    volt=[]
    curr=[]
    try:
        f=open(filename,'r')
    except:
        print(filename+" not found")
        sys.exit()
        
    isDataLine=False
    for line in f.readlines():
        if line[0:9] == 'voltage[V]':
            isDataLine=True
            continue
        if isDataLine:
            rawdata= line.split(' ')
            volt.append(float(rawdata[0]))
            curr.append(float(rawdata[0]))
    resluts = {""
    }

        
if __name__ == '__main__':
    args = get_option()

    read_data(args.fileName)
    serialNumber=args.serialNumber

    code1 = getpass.getpass(prompt='access_code1: ')
    code2 = getpass.getpass(prompt='access_code2: ')

    pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
    pd_client = itkdb.Client( user = pd_user, use_eos = True )

    comps = pd_client.get('getComponent',json={'component' : serialNumber})
    sensorSN=comps['children'][4]['component']['serialNumber']
    pprint.pprint("module : "+serialNumber+" have sensor : "+sensorSN)

    sencomps=pd_client.get('getComponent',json={'component' : sensorSN})
    pprint.pprint(sencomps['tests'][0])
    pprint.pprint(sencomps['tests'][0]['testRuns'][0]['id'])
    senIVtestid=sencomps['tests'][0]['testRuns'][0]['id']
    senIVresults = pd_client.get('getTestRun', json={ "testRun": senIVtestid} )
    pprint.pprint('=========')
    pprint.pprint(senIVresults['results'])
    pprint.pprint('=========')
    senCurrent=senIVresults['results'][1]['value']['current']
    senVoltage=senIVresults['results'][1]['value']['voltage']
    pprint.pprint(senCurrent)
    pprint.pprint(senVoltage)


#    json_open=open(filename,'r')
#    results = json.load(json_open)
#    pprint.pprint(results)
#
#    serialNumber=results['component']
#    uploadStage=results['stage']
#    pprint.pprint(serialNumber)
#
#    code1 = getpass.getpass(prompt='access_code1: ')
#    code2 = getpass.getpass(prompt='access_code2: ')
#
#    pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
#    pd_client = itkdb.Client( user = pd_user, use_eos = True )
#
#    compStage=dbtools.getCompStage(pd_client,serialNumber)
#    dbtools.changestage(pd_client,compStage,uploadStage)
#
#    if dbtools.confirm(results):
#        dbtools.uploadTestRun(pd_client,results)
#
