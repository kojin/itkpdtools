import itkdb
import json
import pprint
import getpass

from pathlib import Path


code1 = getpass.getpass(prompt='access_code1: ')
code2 = getpass.getpass(prompt='access_code2: ')

pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
pd_client = itkdb.Client( user = pd_user, use_eos = True )

print("Test for BareModule Reception")

#componentType='BARE_MODULE'
#testcodelist=['VISUAL_INSPECTION',
#              'MASS_MEASUREMENT',
#              'QUAD_BARE_MODULE_METROLOGY',
#              'FLATNESS',
#              'BARE_MODULE_SENSOR_IV']

#componentType='SENSOR_TILE'
#testcodelist=['IV_MEASURE']
componentType='PCB'
testcodelist=['COMPONENT_POSITION_CHECK',
              'DOWEL_TOLERANCE_CHECK',
              'HV_LV_TEST',
              'IMAGE_CAPTURE_TEST',
              'LAYER_THICKNESS',
              'MASS',
              'METROLOGY',
              'NTC_VERIFICATION',
              'QUICK_TAB_CUTTING_INSPECTION',
              'SIGNAL_TRANSMISSION',
              'SLDO_RESISTORS',
              'VIA_RESISTANCE',
              'VISUAL_INSPECTION',
              'WIREBOND_PULL_TEST']

for acode in testcodelist:
    print("------------")
    print(acode)
    tr_skeleton = pd_client.get( "generateTestTypeDtoSample", json={ 'project':'P', 'componentType':componentType, 'code':acode } )
    with open("examples/example_"+componentType+"_"+acode+".json",'w') as f:
        pprint.pprint(tr_skeleton, stream=f)
    

