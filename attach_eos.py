import itkdb
import json
import pprint
import getpass

from pathlib import Path

code1 = getpass.getpass(prompt='access_code1: ')
code2 = getpass.getpass(prompt='access_code2: ')

pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
pd_client = itkdb.Client( user = pd_user, use_eos = True )

filename="/Users/oide/Downloads/PreProdBareFlexITkv4.1_327647_0101_vi.jpg"

data = {
    "testRun": "64339288d88aa00038b403e9",
    "title": "test_image",
    "description": "a small image shipped with itkdb",
    "url": filename,
    "type": "file"
}

try:

    with Path(filename).open("rb") as fpointer:
        files = {"data": itkdb.utils.get_file_components({"data": fpointer})}
        res = pd_client.post( "createTestRunAttachment", data=data, files=files )
        pprint.pprint( res )
except Exception as e:
    print( e )
    print('failure in uploading')


