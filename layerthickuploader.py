import itkdb
import json
import pprint
import getpass
import sys 
import datetime
import dbtools

from pathlib import Path
from argparse import ArgumentParser

mm=1000

def get_option():
    argparser = ArgumentParser()
    argparser.add_argument('-f', '--fileName', type=str,
                           required=True,
                           help='Input file name')
    argparser.add_argument('-s', '--serialNumber', type=str,
                           required=False,default='',
                           help='specify serial nubmer')
    return argparser.parse_args()


def calclayerthickness(filename,debug=True):
    data=[]
    try:
        f=open(filename,'r')
    except:
        print(filename+" not found")
        sys.exit()

    for line in f.readlines() :
        rawdata= line.split(' ')
        data.append(float(rawdata[2])*mm)
            
    if len(data) != 13 :
        print("data size is wrong ")
        sys.exit()
    else:

        corr=[-5.8,-0.2,3.7,6.1,10.5,12.5,13.4,13.5,11.5,11.9,9.2,5.7,4.7]
        if debug:
            for x in range(len(corr)):
                print(data[x])

        for x in range(len(corr)):
            data[x]=data[x]-corr[x]
            if debug :
                print(data[x])

        if debug :
            pprint.pprint(data)
            print("2-1:",data[2]-data[11])
            print("5-2:",data[5]-data[2])
            print("1-11:",data[1]-data[11])
            print("11:",data[11])
            print("12-11:",data[12]-data[11])
            print("8",data[8])
        return data;
        
    

if __name__ == '__main__':
    #serialNumber='20UPGPQ2601056'
    #with open('/atlaspc22TempSpace/cTest/PreProdBareFlexITkv4.1_327647_0408_Layer_-1/log.txt','r') as f :

    args = get_option()
    data=calclayerthickness(args.fileName)
    serialNumber=args.serialNumber

    if not dbtools.checkSN(serialNumber):
        serialNumber = dbtools.getSNfromFileName(args.fileName)

    dt_now=datetime.datetime.now()
    dt_now_str = dt_now.isoformat(timespec='milliseconds')+'Z'
    print(dt_now);

    results={"component": serialNumber,
             "stage" : "PCB_RECEPTION",
             "date": dt_now_str,
             "institution": "KEK",
             "passed": True,
             "problems": False,
             "properties": {"ANALYSIS_VERSION": "v0",
                            "INSTRUMENT": "",
                            "OPERATOR": "Koji Nakamura"},
             "results": {"BOTTOM_LAYER_THICKNESS": round(data[5]-data[2],5),
                         "COVERLAY_WITH_ADHESIVE_THICKNESS": round(data[12]-data[11],5),
                         "DIELECTRIC_THICKNESS": round(data[11],5),
                         "INNER_LAYER_THICKNESS": round(data[2]-data[11],5),
                         "SOLDERMASK_THICKNESS": round(data[8]-data[9]+4,5),
                         "THICKNESS": round(data[8],5),
                         "TOP_LAYER_THICKNESS": round(data[1]-data[11],5)},
             "runNumber": "1",
             "testType": "LAYER_THICKNESS"}

        
#    pprint.pprint(results)

    if dbtools.confirm(results) :
        code1 = getpass.getpass(prompt='access_code1: ')
        code2 = getpass.getpass(prompt='access_code2: ')
        pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
        pd_client = itkdb.Client( user = pd_user, use_eos = True )
        dbtools.uploadTestRun(pd_client,results)
