import itkdb
import json
import pprint
import getpass
import sys 

code1 = getpass.getpass(prompt='access_code1: ')
code2 = getpass.getpass(prompt='access_code2: ')

pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
pd_client = itkdb.Client( user = pd_user, use_eos = True )

serialNumber=sys.argv[1]

comps = pd_client.get('getComponent',json={'component' : serialNumber})
#pprint.pprint(comps)
compstages=comps['currentStage']
pprint.pprint(compstages)
compStage=compstages['code']
pprint.pprint(compStage)
