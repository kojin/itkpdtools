import itkdb
import json
import pprint
import getpass
import sys 
import datetime


def checkSN(serialNumber):
    if len(serialNumber)==14:
        return True
    else:
        return False


def getSNfromFileName(astring):
    if '20UPG' in astring :
        return astring[astring.rfind('20UPG'):astring.rfind('20UPG')+14]
    else :
        print("string not contain seral nubmer")
        sys.exit()
    

def confirm(results=''):
    dic={'y':True,'yes':True,'n':False,'no':False}
    while True:
        try:
            pprint.pprint(results)
            return dic[input('Do you want to upload data above to ProdDB? [y]es/[n]o >> ').lower()]
        except:
            pass
        print('Error! Input again.')

def uploadTestRun(pd_client,results):
    tr = pd_client.post( 'uploadTestRunResults',  json = results )

def changestage(pd_client,compStage,uploadStage):
    if compStage != uploadStage :
        print("change stage of "+serialNumber+" from "+compStage+" to "+uploadStage)
        cs = pd_client.post( 'setComponentStage', json = { 'component':serialNumber, 'stage': uploadStage } )

def getCompStage(pd_client, serialNumber):
    comps = pd_client.get('getComponent',json={'component' : serialNumber})
    #pprint.pprint(comps)
    compstages=comps['currentStage']
    pprint.pprint(compstages)
    compStage=compstages['code']
    pprint.pprint(compStage)
    return compStage

def shipComp(pd_client, ship_list, sender, recipient, comment=''):
    record = pd_client.post('createShipment', json = { 'name' : comment,
                                                       'sender' : sender,
                                                       'recipient' : recipient,
                                                       'type': 'domestic',
                                                       'shipmentItems' : ship_list } )
    pd_client.post('setShipmentStatus', json = { 'shipment' : record.get('id'),
                                                 'status' : 'inTransit' } )
    pd_client.post('setShipmentStatus', json = { 'shipment' : record.get('id'),
                                                 'status' : 'delivered',
                                                 'shipmentItems' : [ { 'code' : bm.get('code'),
                                                                       'delivered' : True,
                                                                    'damage' : False }
                                                                     for bm in record.get('shipmentItems') ] } )

